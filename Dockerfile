FROM node:12.20-alpine
RUN apk update && apk add python make g++ && rm -rf /var/cache/apk/*
WORKDIR /opt
COPY package*.json process.yml ./
RUN npm install
RUN npm i -g pm2
COPY . .
RUN npm run build
EXPOSE 4000
ENTRYPOINT ["pm2-runtime", "./process.yml"]
