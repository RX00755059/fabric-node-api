# fabric-node-api (version: 2.2.0)
This project setups the gateway and exposes the fabric network as REST API.

## To gether the TLS Certificate of peers/orderer for connection profile.
    awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' <path_to_peers_or_orderers>/tls/ca.crt

# install npm using nvm
    curl -sL https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.0/install.sh -o install_nvm.sh
    bash install_nvm.sh
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

    # restart the terminal
    nvm install v12.13.1
    nvm use v12.13.1
## To install dependency and compile
    npm install
    npm run build

## To update connection profile
## Alias names for IP address
    add names of the ip with the respected local domain name in /etc/hosts file.


## To start the node server
    npm run start

## To build docker image and push to continer registry
    aws ecr get-login-password --region ap-south-1 | docker login --username AWS --password-stdin <account_id>.dkr.ecr.ap-south-1.amazonaws.com

    docker build -t <account_id>.dkr.ecr.ap-south-1.amazonaws.com/<repository_name>:latest .

    docker push <account_id>.dkr.ecr.ap-south-1.amazonaws.com/<repository_name>:latest

## To spin the container
    docker run -d <account_id>.dkr.ecr.ap-south-1.amazonaws.com/<repository_name>:latest -n fabric-node-api -p 4000:4000