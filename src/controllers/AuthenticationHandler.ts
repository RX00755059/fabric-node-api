import * as express from 'express';
import { consoleTestResultHandler } from 'tslint/lib/test';
import { CustomException } from '../exceptions/CustomException';
import { FabricHelper } from '../helpers/FabricHelper';
import { CustomResponse } from '../models/CustomResponse';
import { User } from '../models/User';
import { AuthenticationService } from '../services/AuthenticationService';
import { CommonUtils } from '../utils/CommonUtils';

const jwt = require('jsonwebtoken');

export default function authenticationHandler(app: express.Application) {
    async function registerUser(req: express.Request, res: express.Response)  {
        try {
            let user = new User(req.body);

            if(!user.$name) { throw new CustomException('username is required.'); }
            if(!user.$organization) { throw new CustomException('organization is required.'); }
            
            let response: any = await new FabricHelper().registerAndGetSecret(user.$name);
            if(response.status !== 200 ){ throw new CustomException(response.message); }

            await new FabricHelper().enrollUser(user.$name, response.secret);
            let token = AuthenticationService.generateAccessToken(user);
            response.body = token;

            return res.status(response.status).send(response);
        } catch(error) {
            console.log(error);
            return res.status(CommonUtils.prepareErrorMessage(error).$status).send(CommonUtils.prepareErrorMessage(error));
        }
    }

    async function authUser(req: express.Request, res: express.Response)  {
       try {
            let user = new User(req.body);

            if(!user.$name) { throw new CustomException('username is required.'); }
            if(!user.$organization) { throw new CustomException('organization is required.'); }

            let isUserRegistered = await new FabricHelper().isUserRegistered(user.$name);
            if(!isUserRegistered){ throw new CustomException('user is nor registered. Please register first.'); }

            let token = AuthenticationService.generateAccessToken(user);
            let response: CustomResponse = CustomResponse.createInstance({status: 200, message: 'user logged-in successfully.', body: {token: token}});
            return res.status(response.$status).send(response);
       } catch (error) {
            return res.status(CommonUtils.prepareErrorMessage(error).$status).send(CommonUtils.prepareErrorMessage(error));
       }
    }

    async function reEnrollUser(req: express.Request, res: express.Response)  {
        try {
            let user = new User(req.body);

            if(!user.$name) { throw new CustomException('username is required.'); }
            if(!user.$enrollmentSecret) { throw new CustomException('enrollmentSecret is required.'); }

            await new FabricHelper().enrollUser(user.$name, user.$enrollmentSecret);
            let response: any = { status: 200, message: `user ${user.$name} is re-enrolled successfully.`};
            return res.status(response.status).send(response);

        } catch(error) {
            return res.status(CommonUtils.prepareErrorMessage(error).$status).send(CommonUtils.prepareErrorMessage(error));
        }
    }

    async function enrollAdmin(req: express.Request, res: express.Response)  {
        try {
            
            let response: any = await new FabricHelper().enrollAdmin();
            return res.status(response.status).send(response);

        } catch(error) {
            return res.status(CommonUtils.prepareErrorMessage(error).$status).send(CommonUtils.prepareErrorMessage(error));
        }
    }

    async function reEnrollAdmin(req: express.Request, res: express.Response)  {
        try {
            
            let response: any = await new FabricHelper().reEnrollAdmin();
            return res.status(response.status).send(response);

        } catch(error) {
            return res.status(CommonUtils.prepareErrorMessage(error).$status).send(CommonUtils.prepareErrorMessage(error));
        }
    }

    async function revokeUser(req: express.Request, res: express.Response) {
        try {
            let user = new User(req.body);
            let response: any = await new FabricHelper().revokeUser(user.$name, 'these credentials are compromised.');
            return res.status(response.status).send(response);

        } catch(error) {
            return res.status(CommonUtils.prepareErrorMessage(error).$status).send(CommonUtils.prepareErrorMessage(error));
        }
    }

    app.post('/user/register', registerUser);
    app.post('/user/re-enroll', reEnrollUser);
    app.post('/user/auth', authUser);
    app.post('/user/revoke', revokeUser);

    app.post('/admin/enroll', enrollAdmin);
    app.post('/admin/re-enroll', reEnrollAdmin);
}