import * as express from 'express';
import { CustomResponse } from '../models/CustomResponse';

import log4js = require('log4js');
import { CommonUtils } from '../utils/CommonUtils';
import { ContractHelper } from '../helpers/ContractHelper';
const logger = log4js.getLogger('Common Handler');

export default function commonHandler(app: express.Application) {

    async function customFunction(req: express.Request, res: express.Response) {
        let contractHelper: ContractHelper = new ContractHelper();
        try {
            const contract = await contractHelper.createInstance(req);
            const requestType: string = req.body.requestType;
            const requestName: string = req.body.requestName;
            const args: any = req.body.args;
           
            let responseBuffer: any = null;
            if(requestType == 'get') {
                responseBuffer = await contract.evaluateTransaction(requestName, JSON.stringify(args))
            } else if( requestType =='post') {
                responseBuffer = await contract.submitTransaction(requestName, JSON.stringify(args));
            }

            const responseObj: any = JSON.parse(responseBuffer.toString('utf-8'));
            console.log(JSON.stringify(responseObj))
            
            return res.status(200).send(responseObj);
        } catch (error) {
           return res.status(CommonUtils.prepareErrorMessage(error).$status).send(CommonUtils.prepareErrorMessage(error));
        } finally {
            await contractHelper.disconnect();
        }
    }
    async function getHistoryForKey(req: express.Request, res: express.Response) {
        let contractHelper: ContractHelper = new ContractHelper();
        try {
            const contract = await contractHelper.createInstance(req);

            const responseBuffer: Buffer = await contract.submitTransaction('getHistoryForKey', req.query.identifier);
            const responseObj: any = JSON.parse(responseBuffer.toString('utf-8'));
            console.log(JSON.stringify(responseObj))
            let response: CustomResponse = CustomResponse.createInstance(responseObj);
            return res.status(response.$status).send(response);
        } catch (error) {
           return res.status(CommonUtils.prepareErrorMessage(error).$status).send(CommonUtils.prepareErrorMessage(error));
        } finally {
            await contractHelper.disconnect();
        }
    }

    async function getFilterByCustomQuery(req: express.Request, res: express.Response) {
        let contractHelper: ContractHelper = new ContractHelper();
        try {
            const contract = await contractHelper.createInstance(req);

            const responseBuffer: Buffer = await contract.submitTransaction('getResultsForQueryString', JSON.stringify(req.body));
            const responseObj: any = JSON.parse(responseBuffer.toString('utf-8'));
            console.log(JSON.stringify(responseObj))
            let response: CustomResponse = CustomResponse.createInstance(responseObj);
            return res.status(response.$status).send(response);
        } catch (error) {
           return res.status(CommonUtils.prepareErrorMessage(error).$status).send(CommonUtils.prepareErrorMessage(error));
        } finally {
            await contractHelper.disconnect();
        }
    }

    app.post('/v1/custom-function', customFunction);
    app.get('/v1/history/key', getHistoryForKey);
    app.post('/v1/filter/custom-query', getFilterByCustomQuery)
}