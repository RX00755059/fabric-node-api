import * as express from 'express';
import { CustomResponse } from '../models/CustomResponse';

import log4js = require('log4js');
import { CommonUtils } from '../utils/CommonUtils';
import { ContractHelper } from '../helpers/ContractHelper';
const logger = log4js.getLogger('User Handler');

export default function userHandler(app: express.Application) {
    async function save(req: express.Request, res: express.Response) {
        let contractHelper: ContractHelper = new ContractHelper();
        try {
            const contract = await contractHelper.createInstance(req);
            const responseBuffer: Buffer = await contract.submitTransaction('saveUserDetails', JSON.stringify(req.body));
            const responseObj: any = JSON.parse(responseBuffer.toString('utf-8'));
            console.log(JSON.stringify(responseObj))
            let response: CustomResponse = CustomResponse.createInstance(responseObj);
            return res.status(response.$status).send(response);
        } catch (error) {
           return res.status(CommonUtils.prepareErrorMessage(error).$status).send(CommonUtils.prepareErrorMessage(error));
        } finally {
            await contractHelper.disconnect();
        }
    }

    async function get(req: express.Request, res: express.Response) {
        let contractHelper: ContractHelper = new ContractHelper();
        try {
            const contract = await contractHelper.createInstance(req);
            console.log(req.query.identifier);
            const responseBuffer: Buffer = await contract.evaluateTransaction('getUserDetails', req.query.identifier);
            const responseObj: any = JSON.parse(responseBuffer.toString('utf-8'));
            console.log(JSON.stringify(responseObj))
            let response: CustomResponse = CustomResponse.createInstance(responseObj);
            return res.status(response.$status).send(response);
        } catch (error) {
           return res.status(CommonUtils.prepareErrorMessage(error).$status).send(CommonUtils.prepareErrorMessage(error));
        } finally {
            await contractHelper.disconnect();
        }
    }


    async function saveStepRecord(req: express.Request, res: express.Response) {
        let contractHelper: ContractHelper = new ContractHelper();
        try {
            const contract = await contractHelper.createInstance(req);
            
            const responseBuffer: Buffer = await contract.submitTransaction('saveStepRecord', JSON.stringify(req.body));
            const responseObj: any = JSON.parse(responseBuffer.toString('utf-8'));
            console.log(JSON.stringify(responseObj))
            let response: CustomResponse = CustomResponse.createInstance(responseObj);
            return res.status(response.$status).send(response);
        } catch (error) {
           return res.status(CommonUtils.prepareErrorMessage(error).$status).send(CommonUtils.prepareErrorMessage(error));
        } finally {
            await contractHelper.disconnect();
        }
    }

    async function savePointsHistory(req: express.Request, res: express.Response) {
        let contractHelper: ContractHelper = new ContractHelper();
        try {
            const contract = await contractHelper.createInstance(req);
            
            const responseBuffer: Buffer = await contract.submitTransaction('savePointsHistory', JSON.stringify(req.body));
            const responseObj: any = JSON.parse(responseBuffer.toString('utf-8'));
            console.log(JSON.stringify(responseObj))
            let response: CustomResponse = CustomResponse.createInstance(responseObj);
            return res.status(response.$status).send(response);
        } catch (error) {
           return res.status(CommonUtils.prepareErrorMessage(error).$status).send(CommonUtils.prepareErrorMessage(error));
        } finally {
            await contractHelper.disconnect();
        }
    }

    async function getPointsHistory(req: express.Request, res: express.Response) {
        let contractHelper: ContractHelper = new ContractHelper();
        try {
            const contract = await contractHelper.createInstance(req);
            
            const responseBuffer: Buffer = await contract.evaluateTransaction('getPointsHistory', req.query.identifier);
            const responseObj: any = JSON.parse(responseBuffer.toString('utf-8'));
            console.log(JSON.stringify(responseObj))
            let response: CustomResponse = CustomResponse.createInstance(responseObj);
            return res.status(response.$status).send(response);
        } catch (error) {
           return res.status(CommonUtils.prepareErrorMessage(error).$status).send(CommonUtils.prepareErrorMessage(error));
        } finally {
            await contractHelper.disconnect();
        }
    }

    async function saveHealthScore(req: express.Request, res: express.Response) {
        let contractHelper: ContractHelper = new ContractHelper();
        try {
            const contract = await contractHelper.createInstance(req);
            
            const responseBuffer: Buffer = await contract.submitTransaction('saveHealthScoreDetails', JSON.stringify(req.body));
            const responseObj: any = JSON.parse(responseBuffer.toString('utf-8'));
            console.log(JSON.stringify(responseObj))
            let response: CustomResponse = CustomResponse.createInstance(responseObj);
            return res.status(response.$status).send(response);
        } catch (error) {
           return res.status(CommonUtils.prepareErrorMessage(error).$status).send(CommonUtils.prepareErrorMessage(error));
        } finally {
            await contractHelper.disconnect();
        }
    }

    async function getHealthScore(req: express.Request, res: express.Response) {
        let contractHelper: ContractHelper = new ContractHelper();
        try {
            const contract = await contractHelper.createInstance(req);
            console.log(req.query.date);
            const responseBuffer: Buffer = await contract.evaluateTransaction('getHealthScoreDetails', req.query.identifier, JSON.stringify(req.query.date));
            const responseObj: any = JSON.parse(responseBuffer.toString('utf-8'));
            console.log(JSON.stringify(responseObj))
            let response: CustomResponse = CustomResponse.createInstance(responseObj);
            return res.status(response.$status).send(response);
        } catch (error) {
           return res.status(CommonUtils.prepareErrorMessage(error).$status).send(CommonUtils.prepareErrorMessage(error));
        } finally {
            await contractHelper.disconnect();
        }
    }
    

    app.post('/v1/user-details', save);
    app.get('/v1/user-details', get);
    app.post('/v1/step-records', saveStepRecord);
    app.get('/v1/step-records', saveStepRecord);
    app.post('/v1/points-history', savePointsHistory);
    app.get('/v1/points-history', getPointsHistory);
    app.post('/v1/health-score', saveHealthScore);
    app.get('/v1/health-score', getHealthScore);
}