import * as express from 'express';
import userHandler from './UserHandler';
import authenticationHandler from './AuthenticationHandler';
import commonHandler from './CommonHandler';
import { ContractHelper } from '../helpers/ContractHelper';
import { CustomResponse } from '../models/CustomResponse';
import { CommonUtils } from '../utils/CommonUtils';
export default function entryPoint(app: express.Application) {
    app.get('/logs/health-check', async function (req: express.Request, res: express.Response) {
        let response: any = { status: 200, message: 'Welcome to hyperledger fabric SDK node API.', body: { version: '2.2.0', vendor: { name: 'The Apache Software Foundation'}}};
        return res.status(200).send(response);
    });


    app.post('/api/custom-endpoint', async function (req: express.Request, res: express.Response) {
        let contractHelper: ContractHelper = new ContractHelper();
        try {
            const contract = await contractHelper.createInstance(req);
            const responseBuffer: Buffer = await contract.evaluateTransaction(JSON.stringify(req.body.functionName), JSON.stringify(req.body.body));
            const responseObj: any = JSON.parse(responseBuffer.toString('utf-8'));
            console.log(JSON.stringify(responseObj))
            let response: CustomResponse = CustomResponse.createInstance(responseObj);
            return res.status(response.$status).send(response);
        } catch (error) {
           return res.status(CommonUtils.prepareErrorMessage(error).$status).send(CommonUtils.prepareErrorMessage(error));
        } finally {
            await contractHelper.disconnect();
        }
    });

    userHandler(app);
    authenticationHandler(app);
    commonHandler(app);
}