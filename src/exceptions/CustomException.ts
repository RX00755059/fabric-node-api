export class CustomException implements Error {
    public name: string;
    public message: string;
    public stack?: string;

    constructor(message) {
        this.name = 'CustomException';
        this.message = message;
    }
}