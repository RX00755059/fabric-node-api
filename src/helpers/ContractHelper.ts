import {DefaultEventHandlerStrategies, Gateway, Wallets} from 'fabric-network';
import log4js = require('log4js');
import * as path from 'path';
import { CustomException } from '../exceptions/CustomException';
import { FabricHelper } from './FabricHelper';
const fs = require('fs');

const logger = log4js.getLogger('Contract Helper');
logger.level = 'debug';

export class ContractHelper {
    private gateway: Gateway = null;
    constructor() {
        this.gateway = new Gateway();
    }
    
    async createInstance(req: any){
        const walletPath = await new FabricHelper().getWalletPath();
        const wallet = await Wallets.newFileSystemWallet(walletPath);
        const ccp = await new FabricHelper().getCCP();
        console.log(`Wallet path: ${walletPath}`);
        
        const username = req.user.username;
        const userExists = await wallet.get(username);
        if(!userExists) { throw new CustomException(`An identity of user (${username}) does not exists in the wallet.`); }

        let discoveryOptions = {};
        if(process.env.AS_LOCALHOST == 'true') {  discoveryOptions = { enabled: true, asLocalhost: true }; } 
        else { discoveryOptions = { enabled: true, asLocalhost: false }; }

        let connectionOption = {
            wallet: wallet,
            identity: username,
            discovery: discoveryOptions,
            eventHandlerOptions: {
                strategy: DefaultEventHandlerStrategies.MSPID_SCOPE_ALLFORTX,
            }
        }
        logger.debug('connecting to fabric gateway');
        await this.gateway.connect(ccp, connectionOption);
        const network = await this.gateway.getNetwork(process.env.CHANNEL_NAME);
        return network.getContract(process.env.CONTRACT_NAME);
    }

    async disconnect() {
        logger.debug('disconnecting from fabric gateway');
        this.gateway.disconnect();
    }

}