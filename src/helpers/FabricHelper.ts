import { CustomException } from "../exceptions/CustomException";

const FabricCAServices = require('fabric-ca-client');
var {Wallets} = require('fabric-network');
const path = require('path');
const fs = require('fs');

export class FabricHelper {
    async getCCP() {
        let ccpPath = path.resolve(__dirname, '../../..', 'config', process.env.CURRENT_CCP);
        const ccpJSON = fs.readFileSync(ccpPath, 'utf8')
        const ccp = JSON.parse(ccpJSON);
        return ccp;

    }

    async getCAUrl(ccp: any) {
        return ccp.certificateAuthorities[process.env.CURRENT_CA].url;
    }
    
    async getWalletPath() {
        return path.join(process.cwd(), process.env.CURRENT_WALLET);
    }

    async getRegisterdUser() {
        
    }

    async isUserRegistered(username: string) {
        const walletPath = await this.getWalletPath();
        const wallet = await Wallets.newFileSystemWallet(walletPath);
        
        const userIdentity = await wallet.get(username)
        return userIdentity ? true : false;

    }

    async getCAInfo(ccp: any) {
        return ccp.certificateAuthorities[process.env.CURRENT_CA];
    }

    async getAffiliation() {
        // return `${process.env.CURRENT_ORG}.department1`;
        return process.env.CURRENT_AFFILIATION;
    }
    
    async registerAndGetSecret(username: string) {
        try {
            let ccp = await this.getCCP();

            const caURL = await this.getCAUrl(ccp);
            const ca = new FabricCAServices(caURL);

            const walletPath = await this.getWalletPath();
            const wallet = await Wallets.newFileSystemWallet(walletPath);

            const userIdentity = await wallet.get(username)
            if(userIdentity) {
                throw new CustomException(`An identity for the user ${username} already exists in the wallet`);
            }

            // Checking If admin user is already enrolled
            let adminIdentity = await wallet.get(process.env.CURRENT_REGISTRAR);
            if(!adminIdentity) {
                console.log('An identity for the admin user "admin" does not exist in the wallet');
                await this.enrollAdmin();
                adminIdentity = await wallet.get(process.env.CURRENT_REGISTRAR);
                console.log("Admin Enrolled Successfully")
            }

            const provider = wallet.getProviderRegistry().getProvider(adminIdentity.type)
            const adminUser = await provider.getUserContext(adminIdentity, process.env.CURRENT_REGISTRAR);
            let secret: string = await ca.register({ affiliation: await this.getAffiliation(), enrollmentID: username, role: 'client', maxEnrollments: -1 }, adminUser);
            return { status: 200, message: `${username} registered successfully.`, secret: secret };
        } catch (error) {
            console.log(error);
            return { status: 500, message: error.message }
        }
    }

    async enrollUser(username: string, secret: string) {
        try {
            let ccp = await this.getCCP();

            const caURL = await this.getCAUrl(ccp);
            const ca = new FabricCAServices(caURL);

            const walletPath = await this.getWalletPath();
            const wallet = await Wallets.newFileSystemWallet(walletPath);

            const enrollment = await ca.enroll({enrollmentID: username, enrollmentSecret: secret});
            let x509Identity  = {
                credentials: {
                    certificate: enrollment.certificate,
                    privateKey: enrollment.key.toBytes(),
                },
                mspId: process.env.CURRENT_MSP,
                type: 'X.509',
            }  

            await wallet.put(username, x509Identity);
            return;
        } catch (error) {
            console.log(error);
            return ;
        }
    }

    async enrollAdmin() {
        try {
            let ccp = await this.getCCP();
            const caInfo = await this.getCAInfo(ccp);
            const caTLSCACerts = caInfo.tlsCACerts.pem;
            const ca = new FabricCAServices(caInfo.url, { trustedRoots: caTLSCACerts, verify: false }, caInfo.caName);

            const walletPath = await this.getWalletPath();
            console.log(`walletPath: ${walletPath}`);
            const wallet = await Wallets.newFileSystemWallet(walletPath);
            const identity = await wallet.get(process.env.CURRENT_REGISTRAR);
            if (identity) {
                throw new CustomException(`An identity for the admin user "${process.env.CURRENT_REGISTRAR}" already exists in the wallet`);
            }

            const enrollment = await ca.enroll({ enrollmentID: process.env.CURRENT_REGISTRAR, enrollmentSecret: process.env.CURRENT_REGISTRAR_PW });
            let x509Identity  = {
                credentials: {
                    certificate: enrollment.certificate,
                    privateKey: enrollment.key.toBytes(),
                },
                mspId: process.env.CURRENT_MSP,
                type: 'X.509',
            }  

            await wallet.put(process.env.CURRENT_REGISTRAR, x509Identity);
            return { status: 200, message: `admin is enrolled successfully.`};
        } catch(error) {
            console.log(error);
            return {status: 500, message: error.message};
        }
    }

    async reEnrollAdmin() {
        try {
            let ccp = await this.getCCP();
            const caInfo = await this.getCAInfo(ccp);
            const caTLSCACerts = caInfo.tlsCACerts.pem;
            const ca = new FabricCAServices(caInfo.url, { trustedRoots: caTLSCACerts, verify: false }, caInfo.caName);

            const walletPath = await this.getWalletPath();
            const wallet = await Wallets.newFileSystemWallet(walletPath);
            const identity = await wallet.get(process.env.CURRENT_REGISTRAR);
            if (!identity) {
                console.log(`An identity for the admin user "${process.env.CURRENT_REGISTRAR}" not found to re-enroll.`)
                return;
            }

            const enrollment = await ca.enroll({ enrollmentID: process.env.CURRENT_REGISTRAR, enrollmentSecret: process.env.CURRENT_REGISTRAR_PW });
            let x509Identity  = {
                credentials: {
                    certificate: enrollment.certificate,
                    privateKey: enrollment.key.toBytes(),
                },
                mspId: process.env.CURRENT_MSP,
                type: 'X.509',
            }

            await wallet.put(process.env.CURRENT_REGISTRAR, x509Identity);
            return { status: 200, message: `admin is re-enrolled successfully.`};
        } catch(error) {
            console.log(error);
            return { status: 500, message: error.message};
        }
    }

    async revokeUser(username: string, reason: string) {
        try {
            let ccp = await this.getCCP();
            const caInfo = await this.getCAInfo(ccp);
            const caTLSCACerts = caInfo.tlsCACerts.pem;
            const ca = new FabricCAServices(caInfo.url, { trustedRoots: caTLSCACerts, verify: false }, caInfo.caName);

            const walletPath = await this.getWalletPath();
            const wallet = await Wallets.newFileSystemWallet(walletPath);
            const isAdminEnrolled = await wallet.get(process.env.CURRENT_REGISTRAR);
            if (!isAdminEnrolled) {
                console.log(`An identity for the admin user "${process.env.CURRENT_REGISTRAR}" not found.`);
                return;
            }

            const isUserEnrolled = await wallet.get(username);
            if (!isUserEnrolled) {
                console.log(`An identity for the admin user "${username}" not found to revoke.`);
                return;
            }

            const adminIdentity = await wallet.get(process.env.CURRENT_REGISTRAR);
            const provider = wallet.getProviderRegistry().getProvider(adminIdentity.type)
            const adminUser = await provider.getUserContext(adminIdentity, process.env.CURRENT_REGISTRAR);
            const revokeResponse = await ca.revoke({enrollmentID: username, reason: reason, gencrl: true }, adminUser);

            return { status: 200, message: `user is revoked successfully.`, body: revokeResponse};
        } catch(error) {
            console.log(error);
            return { status: 500, message: error.message};
        }
    }
}