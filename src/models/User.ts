import { Organizations } from "../enums/Organizations";

export class User {
    private name: string;
    private email: string;
    private password: string;
    private organization: Organizations;
    private role: string;
    private countryCode: string;
    private locality: string;
    private departments: string[];
    private departmentRoles: string[];
    private fabricOrg: string;
    private enrollmentSecret: string;

    constructor(user?: any) {
        if(user) {
            this.name = user.name;
            this.email = user.email;
            this.password = user.password;
            this.organization = user.organization;
            this.role = user.role;
            this.countryCode = user.countryCode;
            this.locality = user.locality;
            if(user.departments) {
                this.departments = [];
                for(const department of user.departments ) { this.departments.push(department); }
            }

            if(user.departmentRoles) {
                this.departmentRoles = [];
                for(const role of user.departmentRoles ) { this.departmentRoles.push(role); }
            }
            
            this.fabricOrg = user.fabricOrg;
            this.enrollmentSecret = user.enrollmentSecret;
        }
    }

    /**
     * Getter $name
     * @return {string}
     */
	public get $name(): string {
		return this.name;
	}

    /**
     * Getter $email
     * @return {string}
     */
	public get $email(): string {
		return this.email;
	}

    /**
     * Getter $password
     * @return {string}
     */
	public get $password(): string {
		return this.password;
	}

    /**
     * Getter $organization
     * @return {Organizations}
     */
	public get $organization(): Organizations {
		return this.organization;
	}

    /**
     * Getter $role
     * @return {string}
     */
	public get $role(): string {
		return this.role;
	}

    /**
     * Getter $countryCode
     * @return {string}
     */
	public get $countryCode(): string {
		return this.countryCode;
	}

    /**
     * Getter $locality
     * @return {string}
     */
	public get $locality(): string {
		return this.locality;
	}

    /**
     * Getter $departments
     * @return {string[]}
     */
	public get $departments(): string[] {
		return this.departments;
	}

    /**
     * Getter $departmentRoles
     * @return {string[]}
     */
	public get $departmentRoles(): string[] {
		return this.departmentRoles;
	}

    /**
     * Getter $fabricOrg
     * @return {string}
     */
	public get $fabricOrg(): string {
		return this.fabricOrg;
	}

    /**
     * Getter $enrollmentSecret
     * @return {string}
     */
	public get $enrollmentSecret(): string {
		return this.enrollmentSecret;
	}

    /**
     * Setter $name
     * @param {string} value
     */
	public set $name(value: string) {
		this.name = value;
	}

    /**
     * Setter $email
     * @param {string} value
     */
	public set $email(value: string) {
		this.email = value;
	}

    /**
     * Setter $password
     * @param {string} value
     */
	public set $password(value: string) {
		this.password = value;
	}

    /**
     * Setter $organization
     * @param {Organizations} value
     */
	public set $organization(value: Organizations) {
		this.organization = value;
	}

    /**
     * Setter $role
     * @param {string} value
     */
	public set $role(value: string) {
		this.role = value;
	}

    /**
     * Setter $countryCode
     * @param {string} value
     */
	public set $countryCode(value: string) {
		this.countryCode = value;
	}

    /**
     * Setter $locality
     * @param {string} value
     */
	public set $locality(value: string) {
		this.locality = value;
	}

    /**
     * Setter $departments
     * @param {string[]} value
     */
	public set $departments(value: string[]) {
		this.departments = value;
	}

    /**
     * Setter $departmentRoles
     * @param {string[]} value
     */
	public set $departmentRoles(value: string[]) {
		this.departmentRoles = value;
	}

    /**
     * Setter $fabricOrg
     * @param {string} value
     */
	public set $fabricOrg(value: string) {
		this.fabricOrg = value;
	}

    /**
     * Setter $enrollmentSecret
     * @param {string} value
     */
	public set $enrollmentSecret(value: string) {
		this.enrollmentSecret = value;
	}

}