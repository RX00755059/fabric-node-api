import jwt = require('jsonwebtoken');
import { User } from '../models/User';

export class AuthenticationService {
    static generateAccessToken(user: User) {
        return jwt.sign(
            {
                organization: user.$organization,
                username: user.$name
            },
            process.env.JWT_SECRET, { expiresIn: '24h', algorithm: 'HS256' }
        );

    }
}