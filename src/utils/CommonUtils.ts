import { CustomResponse } from "../models/CustomResponse";

import log4js = require('log4js');
const logger = log4js.getLogger('Common Utils');
export class CommonUtils {
    static generateTimeStamp() {
        return JSON.stringify(new Date());
    }
    static prepareErrorMessage(error: any) {
        switch(error.name) {
            case 'UnauthorisedError':
                return new CustomResponse(401, error.message);
            case 'ReferenceError':
                return new CustomResponse(400, error.message);
            case 'CustomException':
                return new CustomResponse(422, error.message);
            default:
                return new CustomResponse(500, 'Ohh, something went wrong.', error);
        }
    }
}